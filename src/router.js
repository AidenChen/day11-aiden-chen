import TodoList from './pages/todo/TodoList';
import AboutPage from './pages/AboutPage'
import { createBrowserRouter } from "react-router-dom";
import DoneList from './pages/todo/DoneList';
import Layout from './components/Layout';
import TodoDetail from './pages/todo/TodoDetail';
import NotFoundPage from './pages/NotFoundPage'

const router = createBrowserRouter([

    {
        path: "/",
        element: <Layout />,
        children: [
            {
                path: "/",
                element: <TodoList />
            },
            {
                path: "about",
                element: <AboutPage />
            },
            {
                path: "done",
                element: <DoneList />
            },
            {
                path: "todo/:id",
                element: <TodoDetail />
            },
            {
                path: "*",
                element: <NotFoundPage />
            }
        ]
    }
]);

export default router


