import { Card } from "antd"
import '../css/AboutPage.css'
const AboutPage = () => {
    const { Meta } = Card;
    return (
        <div style={{display: 'flex',flexDirection: 'column', alignItems: 'center'}}>
            <span className="aboutSpan">About Page</span>
            <Card hoverable
                style={{
                    width: 240,
                }}
                cover={<img alt="example" src={require('../css/img/about.jpg')} />}>
                <Meta title="Welcome to about page" description="www.xxxxxx.com" />
            </Card>
        </div>
    )
}

export default AboutPage