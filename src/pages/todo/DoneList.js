import TodoGroup from '../../components/TodoGroup'
import { useSelector } from 'react-redux'

const DoneList = () => {
    const doneTodoList = useSelector(state => state.todo.todoList).filter((item) => item.done)


    return (
        <div className="TodoList">
            <span>Done List</span>
            <TodoGroup todoList={doneTodoList} />
        </div>
    )
}

export default DoneList