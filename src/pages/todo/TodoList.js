import TodoGroup from "../../components/TodoGroup"
import TodoGenerator from "../../components/TodoGenerator"
import '../../css/TodoList.css'
import { useSelector } from "react-redux"
import { useEffect } from "react"
import useTodo from "../../hooks/useTodo"

const TodoList = () => {
    const { reloadTodos } = useTodo()
    useEffect(() => {
        reloadTodos()
    }, [reloadTodos])

    const todoList = useSelector(state => state.todo.todoList)

    return (
        <>
            {       
                <div className="TodoList">
                    <span>Todo List</span>
                    <TodoGroup todoList={todoList} />
                    <TodoGenerator />
                </div>
            }
        </>
    )
}

export default TodoList
