import { useParams } from "react-router-dom";
import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { useEffect } from "react";
import "../../css/TodoDetail.css"
const TodoDetail = () => {
    const navigate = useNavigate()
    const params = useParams()
    const todoItem = useSelector(state => state.todo.todoList).find(item => item.id === params.id)
    useEffect(() => {
        if (!todoItem) {
            navigate("/404")
        }
    }, [todoItem, navigate])

    return (
        <>
            {todoItem &&
                <div className="wrapper">
                    <div className="modal">
                        <span className="emoji round">🏆</span>
                        <h1>Congratulations on completing the {todoItem.text} task!!</h1>
                    </div>
                </div>
            }
        </>
    );
}

export default TodoDetail;