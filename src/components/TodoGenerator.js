import { useState } from "react"
import '../css/TodoGenerator.css'
import { createTodo } from '../apis/todo'
import useTodo from "../hooks/useTodo";

const TodoGenerator = () => {
    const ENTER_CODE = 13
    const [inputValue, setinputValue] = useState("")
    const { reloadTodos } = useTodo()
    function changeInputValue(e) {
        setinputValue(e.target.value)
    }
    function add() {
        if (inputValue.trim() === "") {
            alert("can not empty")
            return
        }
        createTodo({
            text: inputValue
        }).then(() => reloadTodos())
        setinputValue("")
    }
    function listenEnter(e) {
        if (e.keyCode === ENTER_CODE) {
            add()
        }
    }
    return (
        <div className="TodoGenerator">
            <input type="text" value={inputValue} onChange={changeInputValue} onKeyUp={listenEnter} placeholder="Todo" />
            <button onClick={add}>add</button>
        </div>
    )
}

export default TodoGenerator