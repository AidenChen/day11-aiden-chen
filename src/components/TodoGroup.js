import TodoItem from "./TodoItem"

const TodoGroup = (props) => {
    return (
        props.todoList.map((element) => {
            return <TodoItem key={element.id} element={element}/>
        })
    )
}

export default TodoGroup