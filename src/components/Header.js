import { NavLink } from "react-router-dom";
import '../css/Header.css'
const Header = () => {
    return (
        <div className="Header">
            <NavLink to="/">Home</NavLink>
            <NavLink to="/done">DoneList</NavLink>
            <NavLink to="/about">About Us</NavLink>
            <hr />
        </div>
    );
}

export default Header;