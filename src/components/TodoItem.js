import "../css/TodoItem.css"
import '../css/iconfont/iconfont.css'
import { useNavigate } from "react-router-dom"
import { useLocation } from 'react-router-dom';
import { toggleTodo } from "../apis/todo"
import { deleteTodo } from "../apis/todo"
import { textTodo } from "../apis/todo";
import useTodo from "../hooks/useTodo";
import { Modal, Input, message } from "antd";
import { useState } from "react";

const TodoItem = (props) => {
    const TODOLIST_PATH = "/"
    let location = useLocation();
    const navigate = useNavigate()
    const { reloadTodos } = useTodo()
    const { TextArea } = Input
    const [messageApi, contextHolder] = message.useMessage();

    const [isModalOpen, setIsModalOpen] = useState(false)
    const [inputValue, setinputValue] = useState(props.element.text)

    const handleOk = () => {
        setIsModalOpen(false);
        textTodo(props.element.id, inputValue).then(() => reloadTodos())
        messageApi.open({
            type: 'success',
            content: 'Update success!',
        });
    };

    function handleCancel() {
        setIsModalOpen(false);
        setinputValue(props.element.text)
    }

    function updateItemDone() {
        if (location.pathname === TODOLIST_PATH) {
            toggleTodo(props.element.id, !props.element.done).then(() => reloadTodos())
        } else {
            navigate(`/todo/${props.element.id}`)
        }
    }

    function deleteItem() {
        if (location.pathname === TODOLIST_PATH) {
            deleteTodo(props.element.id).then(() => reloadTodos())
            messageApi.open({
                type: 'success',
                content: 'Delete success!',
            });
        }
    }

    function editItem() {
        if (location.pathname === TODOLIST_PATH) {
            setIsModalOpen(!isModalOpen)
        }
    }

    function handleTextAreaInput(e) {
        setinputValue(e.target.value)
    }

    return (
        <>
            <div className="todoItem">
                <div className={props.element.done ? 'hline' : 'nline'} onClick={updateItemDone}>{props.element.text}</div>
                <i className="iconfont icon-shanchu1" onClick={deleteItem}></i>
                <i className="iconfont icon-xiugai" onClick={editItem}></i>
                {contextHolder}
                <Modal title="Edit Text" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width="50vh">
                    <TextArea style={{ fontWeight: 'bold', fontSize: '20px', letterSpacing: '2px' }} value={inputValue} onChange={handleTextAreaInput} />
                </Modal>
            </div>
        </>
    )
}

export default TodoItem