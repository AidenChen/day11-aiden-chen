import { createSlice } from "@reduxjs/toolkit";

const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: []
    },
    reducers: {
        initTodoList: (state, action) => {
            state.todoList = action.payload
        }
    }
})


export const { initTodoList } = todoSlice.actions

export default todoSlice.reducer