import { useDispatch } from "react-redux"
import { loadTodos } from "../apis/todo"
import { initTodoList } from "../components/todoSlice"
import { useRef } from "react"

const useTodo = () => {

    const dispatch = useDispatch()

    const reloadTodos = () => {
        loadTodos().then((response) => {
            dispatch(initTodoList(response.data))
        })
    }

    return useRef({ reloadTodos }).current
}

export default useTodo;