import request from "./request"

export const loadTodos = () => {
    return request.get("/todos")
}

export const toggleTodo = (id, done) => {
    return request.put(`/todos/${id}`, {
        done
    })
}

export const textTodo = (id, text) => {
    return request.put(`/todos/${id}`, {
        text
    })
}

export const createTodo = (todo) => {
    return request.post('/todos', todo)
}

export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`)
}