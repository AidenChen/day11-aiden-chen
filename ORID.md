## Objective
- Code View.
- React Router Introduction and Practice.
- Axios & Frontend Call Api.
- Ant-design.

## Reflective
good!

## Interpretive
- Today I have learned a lot of ways to use react and JavaScript, and there is enough practice time in the class to make me more proficient to code.
- Understanding how front-end and back-end interact is a step closer to full-stack engineers.

## Decisional
- It's fun to code in the frontend, keep up the practice!
